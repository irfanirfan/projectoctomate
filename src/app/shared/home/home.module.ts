import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { TopVisitsComponent } from './components/top-visits/top-visits.component';
import { TotalPageViewsComponent } from './components/total-page-views/total-page-views.component';
import { TotalVisitorComponent } from './components/total-visitor/total-visitor.component';
import { BounceRateComponent } from './components/bounce-rate/bounce-rate.component';
import { MapComponent } from './components/map/map.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material/material.module';


@NgModule({
  declarations: [HomeComponent, TopVisitsComponent, TotalPageViewsComponent, TotalVisitorComponent, BounceRateComponent, MapComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FlexLayoutModule,
    MaterialModule
  ]
})
export class HomeModule { }
