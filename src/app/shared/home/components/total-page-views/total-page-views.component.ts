import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-total-page-views',
  templateUrl: './total-page-views.component.html',
  styleUrls: ['./total-page-views.component.scss']
})
export class TotalPageViewsComponent implements OnInit {
  @Input() pageViewLoad:Boolean;
  @Input() totalPageView:number;
  constructor() { }

  ngOnInit() {
  }

}
