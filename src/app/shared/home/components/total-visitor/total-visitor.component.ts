import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-total-visitor',
  templateUrl: './total-visitor.component.html',
  styleUrls: ['./total-visitor.component.scss']
})
export class TotalVisitorComponent implements OnInit {
  @Input() totalVisitors:number
  @Input() visitorLoad:boolean
  constructor() { }

  ngOnInit() {
  }

}
