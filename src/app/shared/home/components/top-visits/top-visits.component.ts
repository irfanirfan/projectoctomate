import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-top-visits',
  templateUrl: './top-visits.component.html',
  styleUrls: ['./top-visits.component.scss']
})
export class TopVisitsComponent implements OnInit {
  @Input() totalVisits:number
  @Input() visitLoad:boolean; 
  constructor() { }

  ngOnInit() {
  }

}
