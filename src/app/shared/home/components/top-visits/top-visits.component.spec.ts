import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopVisitsComponent } from './top-visits.component';

describe('TopVisitsComponent', () => {
  let component: TopVisitsComponent;
  let fixture: ComponentFixture<TopVisitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopVisitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopVisitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
