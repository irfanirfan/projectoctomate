import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bounce-rate',
  templateUrl: './bounce-rate.component.html',
  styleUrls: ['./bounce-rate.component.scss']
})
export class BounceRateComponent implements OnInit {
  @Input() bounceLoad:boolean
  @Input() totalBounceRate:number;
  constructor() { }

  ngOnInit() {
  }

}
