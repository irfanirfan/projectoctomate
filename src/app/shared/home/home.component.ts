import { Component, OnInit, Input } from '@angular/core';
import { MarkerService } from 'src/app/services/marker.service';
import { ProfileService } from 'src/app/services/profile.service';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  totalVisitors: number;
  visitorLoad: boolean = true
  bounceLoad: boolean = true
  pageViewLoad: boolean = true
  visitLoad: boolean = true
  totalVisits: number;
  totalPageView: number;
  totalBounceRate: number;

  constructor(
    private profileService: ProfileService,
    private markerService: MarkerService,
    private dashboardService: DashboardService) { }

  ngOnInit() {
    this.markerService.getData().subscribe(data => {
      console.log(data)
    })
    this.dashboardService.getTotalVisit().subscribe(data => {
      this.totalVisits = data.total || 0
      this.visitLoad = false
    })
    this.dashboardService.getBounceRate().subscribe(data => {
      this.totalBounceRate = data.total || 0
      this.bounceLoad = false
    })
    this.dashboardService.getTotalPageView().subscribe(data => {
      this.totalPageView = data.total || 0
      this.pageViewLoad = false
    })
    setTimeout(() => {
      if (this.totalVisits != 0)
        this.dashboardService.updateTotalVisits(this.totalVisits + 1)
      if (this.totalPageView != 0)
        this.dashboardService.updateTotalPageView(this.totalPageView + 1)
    }, 5000);
    this.profileService.get().subscribe(data => {
      this.totalVisitors = data.length;
      this.visitorLoad = false
    })
  }

}
