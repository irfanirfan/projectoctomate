import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { AuthService } from 'src/app/services/auth.service';
import { ProfileService } from 'src/app/services/profile.service';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { UserModel } from 'src/app/models/user.model';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  fb: FormGroup
  loading: boolean;

  constructor(
    private dialogRef: MatDialogRef<RegisterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private authService: AuthService,
    private profileService: ProfileService,
    private snackbar: MatSnackBar) {
    this.fb = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    })
  }

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }

  onNoClick() {
    this.dialogRef.close(false)
  }

  onSubmit() {
    if (this.fb.valid) {
      this.loading = true
      this.authService.register(this.fb.value).then(data => {
        this.loading = false
        let user = new UserModel
        user = this.fb.value
        user.uid = data.user.uid
        this.profileService.set(user)
        this.snackbar.open("Register Successfully", "X", { duration: 1500, verticalPosition: 'top' })
        this.dialogRef.close(true)
      }).catch(error => {
        console.log(error)
        this.loading = false
        this.snackbar.open(error, "X", { duration: 2500, verticalPosition: 'top' })
      })
    }
  }

}
