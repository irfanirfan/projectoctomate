import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserModel } from '../models/user.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  userCollection = this.db.collection('users')
  constructor(private db: AngularFirestore) { }

  get(): Observable<any> {
    return this.userCollection.valueChanges()
  }

  set(user: UserModel) {
    return this.userCollection.doc<UserModel>(`${user.uid}`).set(user)
  }

  update(user: UserModel) {
    return this.userCollection.doc<UserModel>(`${user.uid}`).update(user)
  }

  delete(user: UserModel) {
    return this.userCollection.doc<UserModel>(`${user.uid}`).delete()
  }
}
