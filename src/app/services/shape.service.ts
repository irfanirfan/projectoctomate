import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShapeService {

  constructor(private httpClient: HttpClient) { }

  getStateShapes(): Observable<any> {
    return this.httpClient.get('/assets/data/us-states.geo.json')
  }
}
