import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopUpService {

  constructor() { }

  makeCapitalPopup(data: any): string {
    return `` +
      `<div>Capital: ${data.NAME}</div>` +
      `<div>Population: ${data.CENSUSAREA}</div>`
  }
}
