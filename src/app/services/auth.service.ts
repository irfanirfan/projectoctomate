import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: UserModel;
  constructor(private db: AngularFirestore, private afAuth: AngularFireAuth, private router: Router) {
    this.afAuth.authState.subscribe(user => {
      if (user != null) {
        this.user = new UserModel
        this.user.uid = user.uid
        this.user.displayName = user.displayName
        this.user.email = user.email
        localStorage.setItem("user", JSON.stringify(this.user))

      } else {
        localStorage.setItem('user', null);
        this.router.navigate(['']);
      }
    }, error => {
      console.log(error)
      this.router.navigate(['']);
    })
  }

  getUser() {
    return this.user = JSON.parse(localStorage.getItem("user"))
  }

  login(user) {
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
  }

  googleSignIn() {
    return this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
  }

  facebookSignIn() {
    return this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider())
  }

  twitterSignIn() {
    return this.afAuth.auth.signInWithPopup(new auth.TwitterAuthProvider())
  }

  logout() {
    return this.afAuth.auth.signOut()
  }

  register(user) {
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
  }

  forgotPassword(email: string) {
    return this.afAuth.auth.sendPasswordResetEmail(email)
  }
}
