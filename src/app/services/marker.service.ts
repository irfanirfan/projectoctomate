import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as L from 'leaflet';
import { PopUpService } from './pop-up.service';

@Injectable({
  providedIn: 'root'
})
export class MarkerService {

  static ScaledRadius(val: number, maxVal: number): number {
    return 20 * (val / maxVal);
  }
  capitals: string = '/assets/data/us-country.geo..json';

  constructor(private http: HttpClient, private popupService: PopUpService) {
  }

  makeCapitalMarkers(map: L.map): void {
    this.http.get(this.capitals).subscribe((res: any) => {
      for (const c of res.features) {
        let lat = c.geometry.coordinates[0][0][0][0];
        let lon = c.geometry.coordinates[0][0][0][1];
        if (lat == null)
          lat = c.geometry.coordinates[0][0][0];
        if (lon == null)
          lon = c.geometry.coordinates[0][0][1];
        const marker = L.marker([lon, lat]);
        marker.bindPopup(this.popupService.makeCapitalPopup(c.properties));
        marker.addTo(map);
      }
    });
  }

  getData(){
    return this.http.get(this.capitals)
  }

  makeCapitalCircleMarkers(map: L.map): void {
    this.http.get(this.capitals).subscribe((res: any) => {
      const maxVal = Math.max(...res.features.map(x => x.properties.CENSUSAREA), 0);
      for (const c of res.features) {
        let lat = c.geometry.coordinates[0][0][0][0];
        let lon = c.geometry.coordinates[0][0][0][1];
        if (lat == null)
          lat = c.geometry.coordinates[0][0][0];
        if (lon == null)
          lon = c.geometry.coordinates[0][0][1];

        const circle = L.circleMarker([lon, lat], {
          radius: MarkerService.ScaledRadius(c.properties.CENSUSAREA, maxVal)
        });
        circle.bindPopup(this.popupService.makeCapitalPopup(c.properties));
        circle.addTo(map);
      }
    });
  }
}
