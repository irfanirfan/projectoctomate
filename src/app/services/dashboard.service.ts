import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  dashboardCollection = this.db.collection('dashboard')
  constructor(private db: AngularFirestore) { }

  getTotalVisit(): Observable<any> {
    return this.dashboardCollection.doc('totalvisits').valueChanges()
  }

  getTotalPageView(): Observable<any> {
    return this.dashboardCollection.doc('totalpageview').valueChanges()
  }

  getBounceRate(): Observable<any> {
    return this.dashboardCollection.doc('bouncerate').valueChanges()
  }

  updateTotalVisits(data) {
    return this.dashboardCollection.doc('totalvisits').update({total:data})
  }

  updateTotalPageView(data) {
    return this.dashboardCollection.doc('totalpageview').update({total:data})
  }

  updateBounceRate(data) {
    return this.dashboardCollection.doc('bouncerate').set({total:data})
  }


}
