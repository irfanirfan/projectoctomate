import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher, MatSnackBar, MatDialog } from '@angular/material';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { RegisterComponent } from 'src/app/shared/modals/register/register.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})



export class LoginComponent implements OnInit {
  fb: FormGroup;
  loading: boolean
  constructor(
    public authService: AuthService,
    private router: Router,
    private snackbar: MatSnackBar,
    private dialog: MatDialog) {
    this.fb = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
    if (this.authService.getUser() != null) {
      this.router.navigate(['/dashboard'])
    }
  }

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }

  ngOnInit() {

  }

  onLogin() {
    if (this.fb.valid) {
      this.loading = true
      this.authService.login(this.fb.value).then(() => {
        this.loading = false
        this.snackbar.open("Login Successfully", "X", { duration: 1500, verticalPosition: 'top' })
        this.router.navigate(['/dashboard'])
      }).catch(error => {
        console.log(error)
        this.loading = false
        this.snackbar.open("Login Failed, Please Try Again Later", "X", { duration: 2500, verticalPosition: 'top' })
      })
    }
  }

  onSignInGoogle() {
    this.authService.googleSignIn().then(() => {
      location.reload()
    })
  }

  onSignInFacebook() {
    this.authService.facebookSignIn().then(() => {
      location.reload()
    })
  }

  onSignInTwitter() {
    this.authService.twitterSignIn().then(() => {
      location.reload()
    })
  }

  onRegister() {
    let dialogRef = this.dialog.open(RegisterComponent, {
      width: '350px'
    })
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.router.navigate(['/dashboard'])
      }
    })
  }

  onResetPassword(email: string) {
    if (email != null) {
      this.authService.forgotPassword(email).then(() => {
        this.snackbar.open("Please Check Your Email for reset Password", "X", { duration: 1500, verticalPosition: 'top' })
      }).catch(error => {
        console.log(error)
        this.snackbar.open(error, "X", { duration: 2500, verticalPosition: 'top' })
      })
    }
  }

}
