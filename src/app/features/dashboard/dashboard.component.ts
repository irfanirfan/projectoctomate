import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { fadeAnimation } from 'src/app/shared/animations';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [fadeAnimation]
})
export class DashboardComponent implements OnInit {
  screenWidth: number;
  visible: boolean;
  menus = [];
  constructor(
    public authService: AuthService,
    private dashboardService:DashboardService,
    private router: Router) {
    this.screenWidth = window.innerWidth
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    }
  }

  ngOnInit() {
    this.menus = [
      {
        routerLink: "home",
        icon: 'dashboard',
        name: "Dashboard",
      },
      {
        routerLink: "email",
        icon: 'email',
        name: "Email"
      },
      {
        routerLink: "",
        icon: 'message',
        name: "Compose"
      },
      {
        routerLink: "",
        icon: 'calendar_today',
        name: "Calendar"
      },
    ]
    

  }

  onLogout() {
    // this.dashboardService.updateBounceRate()
    this.authService.logout()
    localStorage.removeItem("user")
    this.router.navigate(['login'])
  }

}
