import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { MaterialModule } from 'src/app/shared/material/material.module';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    MaterialModule,
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
