import { Component } from '@angular/core';
import { fadeAnimation } from 'src/app/shared/animations';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})
export class AppComponent {
  title = 'ProjectOctomate';

  getPage(outlet) {
    return outlet.activatedRouteData['page'] || 'one';
  }
}
