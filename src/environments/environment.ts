// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDGxu88PZvg10wF8AtdxCASgJYq3MoMCsk",
    authDomain: "octomate-514b4.firebaseapp.com",
    databaseURL: "https://octomate-514b4.firebaseio.com",
    projectId: "octomate-514b4",
    storageBucket: "octomate-514b4.appspot.com",
    messagingSenderId: "381018308969",
    appId: "1:381018308969:web:46402196a277769fbcfdf8",
    measurementId: "G-3EDF193Z1K"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
